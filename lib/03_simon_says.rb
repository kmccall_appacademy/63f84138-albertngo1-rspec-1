def echo(echo)
  echo
end

def shout(shout)

  shout.upcase

end

def repeat(string, times = 2)

  ([string] * times).join(" ")

end

def start_of_word(string, number)

  string[0...number]

end

def first_word(string)

  string.split(" ").first

end

def titleize(str)

  little_words = %w(and over the)

  str.capitalize!

  str_split = str.split(" ")

  str_split.map! do |word|
    if little_words.include?(word)
      word
    else
      word.capitalize
    end
  end
  
  str_split.join(" ")

end
