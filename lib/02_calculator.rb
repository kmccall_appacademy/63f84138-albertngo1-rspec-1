def add(a, b)

  a + b

end

def subtract(a, b)

  a - b

end

def sum(arr)
  sum = 0

  arr.each { |number| sum += number }

  sum

end

def multiply(*args)

  args.reduce(:*)

end

def power(base, power)

  base ** power

end

def factorial(n)

  return 1 if n == 0

  (1..n).reduce(:*)

end
