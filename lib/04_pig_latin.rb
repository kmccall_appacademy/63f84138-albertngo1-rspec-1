def translate(sentence)

vowels = %w(a e i o u)

words = sentence.split(" ")

words.map! do |word|

  until vowels.include?(word[0]) && word[-1] != 'q'

    word = word[1..-1] + word[0]

  end

  word << 'ay'
end

words.join(" ")

end
